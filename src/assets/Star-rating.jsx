import { StarIcon } from "@chakra-ui/icons";
import React, { useState } from "react";

const StarRating = () => {
  const [rating, setRating] = useState(null);
  const [hoveredStars, setHoveredStars] = useState([]);

  const handleStarClick = (num) => {
    setRating(num);
  };

  const handleStarHover = (ratingValue) => {
    const stars = [];
    for (let i = 1; i <= ratingValue; i++) {
      stars.push(i);
    }
    setHoveredStars(stars);
  };

  const resetStarHover = () => {
    setHoveredStars([]);
  };

  const setEmoji = () => {
    switch (rating) {
      case 1:
        return "🥲";
      case 2:
        return "😞";
      case 3:
        return "😐";
      case 4:
        return "😃";
      case 5:
        return "😎";
      default:
        return "";
    }
  };

  return (
    <div>
      {[...Array(5)].map((star, i) => {
        const ratingValue = i + 1;
        return (
          <span
            id="main"
            key={ratingValue}
            onClick={() => handleStarClick(ratingValue)}
            onMouseEnter={() => handleStarHover(ratingValue)}
            onMouseLeave={() => resetStarHover()}
            style={{
              color:
                ratingValue <= (rating || 0) ||
                hoveredStars.includes(ratingValue)
                  ? "yellow"
                  : "gray",
            }}
          >
            <StarIcon />
          </span>
        );
      })}
      <p>{setEmoji()}</p>
    </div>
  );
};

export default StarRating;
