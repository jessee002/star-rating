import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import StarRating from './assets/Star-rating'

function App() {
  return (
    <div className="App">
      <StarRating />
    </div>
  );
}

export default App
